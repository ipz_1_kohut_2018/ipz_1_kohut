﻿using System.Data.Entity;
using Airport.Models;

namespace Airport.DataAccess.Repositories
{
    public class AirportContext : DbContext
    {
        public AirportContext() : base("AirportDB")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AirportContext>());
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Ticket> Tickets { get; set; }

    }
}

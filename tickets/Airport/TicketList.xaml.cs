﻿using System.Linq;
using System.Windows;
using Airport.DataAccess.Repositories;
using Airport.Models;

namespace Airport
{
    public partial class TicketList : Window
    {
        public TicketList(Client client)
        {
            InitializeComponent();
            using (AirportContext context = new AirportContext())
            {
                var tickets = context.Tickets.ToList();

                foreach (Ticket ticket in tickets)
                {
                    if (client.Login == ticket.Login && client.Password == ticket.Password)
                    {
                        TicketBlock.Text += " From: " + ticket.From + "    To: " + ticket.Destination + "    Distance: " + ticket.Distance + "\n Arrival: " + ticket.Arrival + "    Departure: " + ticket.Departure + "\n Seat: " + ticket.Seat + "    Plane # " + ticket.Coach + "\n Price: " + ticket.Price + "grn\n\n";
                    }
                }

            }
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Airport.DataAccess.Repositories;
using Airport.Models;

namespace Airport
{
    /// <summary>
    /// Interaction logic for RegisterWindow.xaml
    /// </summary>
    public partial class RegisterWindow : Window
    {
        public RegisterWindow()
        {
            InitializeComponent();
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            if (FirstName.Text.Length == 0 || LastName.Text.Length == 0 || Phone.Text.Length == 0 || Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");

            }
            else
            {

                using (AirportContext repository = new AirportContext())
                {
                    Client client = new Client(FirstName.Text, LastName.Text, Phone.Text, Login.Text, Password.Password.ToString());
                    repository.Clients.Add(client);

                    repository.SaveChanges();
                    MainWindow main = new MainWindow(client);
                    main.Show();
                    Close();
                }
                Close();
            }
        }
    }
}

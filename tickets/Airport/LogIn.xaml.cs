﻿using System.Linq;
using System.Windows;
using Airport.DataAccess.Repositories;
using Airport.Models;

namespace Airport
{
    /// <summary>
    /// Interaction logic for LogIn.xaml
    /// </summary>
    public partial class LogIn : Window
    {
        public LogIn()
        {
            InitializeComponent();
        }

        private void LogIn_Click(object sender, RoutedEventArgs e)
        {
            if (Login.Text.Length == 0 || Password.Password.Length == 0)
            {
                MessageBox.Show("Fill all fields!");
            }
            else
            {
                using (AirportContext repository = new AirportContext())
                {
                    var clients = repository.Clients.ToList();
                    int counter = clients.Count;
                    Client c = new Client();
                    foreach (var client in clients)
                    {
                        if (client.Login != Login.Text || client.Password != Password.Password.ToString())
                        {
                            counter--;
                        }
                        else
                        {
                            c = client;
                        }
                    }
                    if (counter == 0)
                    {
                        MessageBox.Show("Incorrect login or password!");
                    }
                    else
                    {
                        MainWindow main = new MainWindow(c);
                        main.Show();
                        Close();
                    }
                }
            }
        }

        private void Reg_Click(object sender, RoutedEventArgs e)
        {
            RegisterWindow reg = new RegisterWindow();
            reg.Show();
            Close();
        }
    }
}
